#WijmoJS

**官网**：https://www.grapecity.com.cn/developer/wijmojs

**下载**：https://www.grapecity.com.cn/download/?pid=54

**社区**：https://gcdn.grapecity.com.cn/showforum-173-1.html

**演示**：https://demo.grapecity.com.cn/wijmo/demos/

![葡萄城控件微信服务号](https://images.gitee.com/uploads/images/2019/0612/084722_83196c02_103682.png "葡萄城控件微信服务号")

----------


# WijmoJS 是什么？ #

WijmoJS 前端开发工具包由多款灵活高效、零依赖、轻量级的纯前端控件组成，如表格控件 FlexGrid、图表控件 FlexChart、数据分析 OLAP 等，完美支持原生 JavaScript，以及 Angular、React、Vue、TypeScript、Knockout 和 Ionic 等框架，可用于企业快速构建桌面、移动 Web 应用程序。
借助葡萄城深厚的技术底蕴，WijmoJS 致力于为各领域用户提供更稳定、更高效的前端开发工具。产品自面市以来，已在招商银行、微软 Dynamics 项目、思科、特斯拉、富士通等知名企业中得以成功应用。WijmoJS 凭借其先进的体系架构、简单易学的使用文档、超过 500 种 Demo 演示、顶级的控件性能，以及轻松、易用的操作体验，可全面满足企业前端开发所需，是构建企业级 Web 应用程序最高效的纯前端开发工具包。

# WijmoJS 包含了什么？ #

![](https://images.gitee.com/uploads/images/2019/0612/085201_056de61b_103682.png)

# 为什么要选择WijmoJS？ #

## 触控操作支持优先，移动终端支持优先 ##

WijmoJS从最初的设计开始就支持移动终端的浏览器。对于每一个WijmoJS控件而言，自适应式布局设计以及触摸支持是最主要的设计考虑因素。
![](http://i.imgur.com/W2ivWS3.png)


## 一流的支持Angular ##

Angular JS是当今最流行，最强大的应用程序框架之一。我们相信，它将继续得到普及，并将作为Web开发的下一个方向。正因为如此，我们发布并维护的全部的控件中将全面支持Angular JS，此外，我们所提供的大部分Sample都将使用Angular JS。当然，您也可以将Wijmo和其他的Web框架配合使用，如果我们的客户需要，我们将逐步的增加对于其他框架的官方支持。

![](http://i.imgur.com/FL4um3Z.png)


## 真正的JavaScript控件 ##

ECMAScript 5标准添加了对于属性getter和setter的支持。这可能看起来只是一个小小的变化，但是它将带来很大的不同。它给了我们机会去创建真正的JavaScript控件。举个例子，之前我们必须写这样的代码将一个属性的值加1：control.value(control.value() + 1)，而现在，您可以直接写成control.value++。

我们希望为从.NET平台迁移过来的开发人员提供熟悉的编程体验。正因如此，我们在JavaScript中间提供了ICollectionView类型。全部的Wijmo控件支持绑定到CollectionView，以便向开发人员提供一种方便的从.NET平台迁移过来的途径。
![](http://i.imgur.com/FcGcjQA.png)


## 高性能、轻量级 ##

在WijmoJS版本，我们有机会彻底地重新审视并重写我们的控件。我们选择以现代浏览器作为控件支持的目标，这将使得我们可以创建市面上最快、最轻量级的控件。每一个控件都在尺寸和速度上做了尽可能的优化。

![](http://i.imgur.com/E8pC66g.png)

## 灵活的API ##

这一点已经在它的名字中间有所体现！您可能记得我们曾经在数个不同的平台发布的著名的FlexGrid，即便是回溯到VB时代也有它的身影。我们将此灵活的“Flex”模型以及FlexGrid带到了JavaScript。正是由于它具有简单却又非常灵活的API，使得FlexGrid变得难以置信的流行。该设计的核心思想是开发一个具有多个扩展点的简单控件，使得开发人员可以按照需要扩展并增强其功能。这也是使得FlexGrid以及FlexChart如此的灵活和强大的原因。

![](http://i.imgur.com/jQ6fdzY.png)



----------

更新日期：

1.2019/5/6 wijmo-5.20191.603（正式版）